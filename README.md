## Customized `jupyter` containers

Launching them through `docker-compose` makes it simply and quick. Just type:

```bash
docker-compose up <service>
```
Currently there is just one service specified in the `docker-compose.yml` file, therefore no need to specify `<service>`. But in the future I may add additional services to launch different containers.

To stop:

```bash
docker-compose down
```
